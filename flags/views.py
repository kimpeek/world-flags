import json
from random import choice, sample

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView

from .models import FlagModel


class FlagTemplateView(TemplateView):
    template_name = 'flags/guess.html'


@csrf_exempt
def get_question_set(request):
    """Serve one image url and 4 country names, one of which matches the image

    This method requests all records in the DB and
    uses Python to select the required 4 random images.
    It is more efficient to use SQL on larger collections,
    but this is the simpler option. The proper method
    requires 2 DB queries to be optimal.
    """

    flags = FlagModel.objects.all()
    flag_pool = sample(list(flags), 4)
    response = {
        'image': choice(flag_pool).image,
        'choices': [f.country for f in flag_pool]
    }
    return JsonResponse(response)


@csrf_exempt
def get_matching_country(request):
    image = json.loads(request.body).get('image')
    country = FlagModel.objects.filter(image=image)[0].country
    return JsonResponse({'country': country})
