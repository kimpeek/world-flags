from django.db import models


class FlagModel(models.Model):
    image = models.CharField(max_length=120)
    country = models.CharField(max_length=120)

    def __str__(self):
        return f'{self.country} - {self.image}'
