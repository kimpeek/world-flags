const base_url = 'https://worldflagsguess.herokuapp.com';
const img_base = base_url + '/static/flags/images/';
const question_url = base_url + '/question/';
const country_url = base_url + '/country/';



// ----- USER ACTIONS -----
function on_page_load() {
    const question_set = request_question();
    load_question(question_set);
    add_event_listeners();
}


function select_country() {
    const guess = this.innerText;
    const image = document.getElementById('flag-image').src.split('/').slice(-1)[0];
    const response = get_image_country({'image': image});
    const image_country = response.country;
    if (image_country === guess) {
        set_prompt('correct', image_country);
        toggle_buttons();
        increment_score('correct');
    } else {
        set_prompt('incorrect', image_country);
        toggle_buttons();
        increment_score('incorrect');
    }
}


function request_next_flag() {
    const question_set = request_question();
    load_question(question_set);
    set_prompt('reset');
    toggle_buttons();
}


// ----- HELPER FUNCTIONS -----
function request_question() {
    const request = new XMLHttpRequest();
    request.open('GET', question_url, false);
    request.send();
    return JSON.parse(request.response)
}


function load_question(question_set) {
    document.getElementById('flag-image').src = img_base + question_set.image;
    const buttons = document.getElementsByClassName('option');
    for (const choice in question_set.choices) {
        buttons[choice].innerText = question_set.choices[choice];
    }
    set_prompt('reset')
}


function add_event_listeners() {
    document.getElementById('next').addEventListener('click', request_next_flag);
    const option_buttons = document.getElementsByClassName('option');
    for (const button in option_buttons) {
        try {
            option_buttons[button].addEventListener('click', select_country)
        } catch(e) {
            // ignore non-buttons included in HTMLCollection
        }
    }
}


function set_prompt(status, country=null) {
    const prompts = {
        'reset': {
            'prompt': "Which country's flag is this?",
            'class': "neutral",
        },
        'correct': {
            'prompt': `Correct! It's ${country}'s flag!`,
            'class': 'correct'
        },
        'incorrect': {
            'prompt': `Incorrect. It's ${country}'s flag.`,
            'class': 'incorrect'
        },
    };
    document.getElementById('prompt-text').innerText = prompts[status].prompt;
    document.getElementById('prompt').className = prompts[status].class;
}


function get_image_country(data) {
    if (typeof data !== "string") {
        data = JSON.stringify(data);
    }
    const request = new XMLHttpRequest();
    request.open('POST', country_url, false);
    request.send(data);
    return JSON.parse(request.response)
}


function toggle_buttons() {
    const options = document.getElementsByClassName('option');
    for (const option in options) {
        const button = options[option];
        try {
            if (button.style.display === 'none') {
                button.style.display = 'block'
            } else {
                button.style.display = 'none'
            }
        } catch(e) {

        }
    }
    const next = document.getElementById('next');
    if (next.style.display === 'none') {
        next.style.display = 'block'
    } else {
        next.style.display = 'none'
    }
}


function increment_score(element_id) {
    // Animate the box
    const animations = ['bounce', 'flash', 'pulse', 'rubberBand', 'shake', 'headShake', 'swing', 'tada', 'wobble', 'jello', 'bounceIn', 'fadeIn', 'flipInX', 'flipInY', 'hinge', 'jackInTheBox', 'heartBeat'];
    const animation = animations[Math.floor(Math.random()*animations.length)];
    animate_element(element_id + '-score-box', animation);

    // Update the score
    const score = document.getElementById(element_id + '-score');
    const current = parseInt(score.innerText);
    score.innerText = current + 1;
}


function animate_element(element_id, animation) {
    // Add classes
    const element = document.getElementById(element_id);
    element.classList.add('animated', animation);

    // Remove classes after animation
    const animation_endings = [
        'animationend',
        'oAnimationEnd',
        'mozAnimationEnd',
        'webkitAnimationEnd',
    ];
    for (const ending in animation_endings) {
        element.addEventListener(animation_endings[ending], function() {
            element.classList.remove('animated', animation);
        })
    }
}


on_page_load();
