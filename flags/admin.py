from django.contrib import admin

from .models import FlagModel

admin.site.register(FlagModel)
